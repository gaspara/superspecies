-- Add a synonym
INSERT INTO st_speciesnames (species_name) VALUES ('Dendrocoptes medius');
INSERT INTO st_speciesnames (species_name) VALUES ('Picus medius');

INSERT INTO st_namestatus (species_name,namestatus) VALUES ('Leiopicus medius','synonym for Dendrocopos medius');
INSERT INTO st_namestatus (species_name,namestatus) VALUES ('Dendrocoptes medius','synonym for Dendrocopos medius');
INSERT INTO st_namestatus (species_name,namestatus) VALUES ('Picus medius','synonym for Dendrocopos medius');
--INSERT INTO st_namestatus (species_name,namestatus) VALUES ('Dendrocopos medius','accepted name');

INSERT INTO st_speciesnames (species_name) VALUES ('Cyanistes caeruleus');
INSERT INTO st_namestatus (species_name,namestatus) VALUES ('Cyanistes caeruleus','accepted name');
UPDATE st_namestatus SET namestatus='synonym for Cyanistes caeruleus' WHERE species_name='Parus caeruleus';


-- Add a magyar_nev
UPDATE st_magyarnevek SET magyarnev = magyarnev || '{erdei vöröshangya}' WHERE species_name='Formica rufa';
